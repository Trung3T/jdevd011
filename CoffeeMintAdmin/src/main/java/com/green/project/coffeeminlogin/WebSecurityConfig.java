package com.green.project.coffeeminlogin;

import org.springframework.security.core.userdetails.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import com.green.project.coffeeminlogin.services.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//	@Override
//	protected UserDetailsService userDetailsService() {
//		UserDetails user1 = User.withUsername("admin")
//				.password("7f4f1512-48ac-4a30-a537-29da6e1825f0")
//				.roles("ADMIN").build();
//		
//		return new InMemoryUserDetailsManager(user1);
//	}

//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//			.passwordEncoder(new BCryptPasswordEncoder())
//			.withUser("admin")
//			.password("$2a$10$FoODiG55Yt2eDgUA5mEmJexgZ3yiMAiZWE5tmRzocndZhs0K9B1ku")
//			.roles("ADMIN")
//			.and()
//			.withUser("greena")
//			.password("$2a$10$FoODiG55Yt2eDgUA5mEmJexgZ3yiMAiZWE5tmRzocndZhs0K9B1ku")
//			.roles("USER");
//	}

	@Bean
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceImpl();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/", "/css/**", "/font/**", "/images/**", "/js/**", "/vendor/**").permitAll()
		.antMatchers("/edit/*", "/delete/*", "/new").hasAnyAuthority("ADMIN")
		.anyRequest().authenticated()
		.and().formLogin().permitAll()
		.and().logout().permitAll();

//		http.authorizeRequests().anyRequest().permitAll();
		/* */
//		http.authorizeRequests()
//			.anyRequest().authenticated()
//			.and()
//			.formLogin().permitAll()
//			.and()
//			.logout().permitAll();
	}

}
