package com.coffeemintt.client.helpers;

public enum AuthProvider {
	BASIC,
	FACEBOOK,
	GOOGLE
}
