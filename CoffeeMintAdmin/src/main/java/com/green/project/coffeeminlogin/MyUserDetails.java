package com.green.project.coffeeminlogin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.green.project.coffeeminlogin.models.Role;
import com.green.project.coffeeminlogin.models.User;

public class MyUserDetails implements UserDetails {

	private static final long serialVersionUID = -6552614711849325534L;

	private User user;
	
	public MyUserDetails(User user) {
		this.user = user;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		Set<Role> roles = user.getRoles();
		System.out.println("authorities user name:: " + user.getUsername());
		for(Role role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
			System.out.println("authorities user roles:: " + role.getName());
		}
		return authorities;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return user.getEnabled();
	}

}
