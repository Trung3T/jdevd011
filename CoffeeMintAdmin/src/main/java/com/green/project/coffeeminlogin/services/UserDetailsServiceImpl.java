package com.green.project.coffeeminlogin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.green.project.coffeeminlogin.MyUserDetails;
import com.green.project.coffeeminlogin.dao.UserRepository;
import com.green.project.coffeeminlogin.models.User;

public class UserDetailsServiceImpl implements UserDetailsService {
	///Push this file
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) 
			throws UsernameNotFoundException {
		User user = userRepository.getUserByUserName(username);
		
		if (user == null) {
			throw new UsernameNotFoundException("Ten dang nhap khong ton tai");
		}
		
		return new MyUserDetails(user);
	}
}
