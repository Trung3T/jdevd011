package com.green.project.coffeeminlogin.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.green.project.coffeeminlogin.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
