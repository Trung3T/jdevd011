package com.green.project.coffeeminlogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoffeeMintLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoffeeMintLoginApplication.class, args);
	}

}
